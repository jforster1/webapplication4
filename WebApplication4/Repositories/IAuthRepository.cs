using System.Threading.Tasks;
using WebApplication4.Models;

namespace WebApplication4.Repositories
{
    public interface IAuthRepository
    {
        Task<TblUser> Register(TblUser user, string password);
        Task<TblUser> Login(string username, string password);
        Task<bool> UserExists(string username);
    }
}