using WebApplication4.Dtos;
using WebApplication4.Models;
using AutoMapper;

namespace WebApplication4.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
//            CreateMap<TblBook, BookListDto>();
            CreateMap<LoginDto, TblUser>();
            CreateMap<RegisterDto, TblUser>();
        }
    }
}